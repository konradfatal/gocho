//The main package and application entry point routine
package main

import (
	log "code.google.com/p/log4go"
	"server"
    "fmt"
)

func main() {
	log.LoadConfiguration("log4go.xml")
	s := server.New(4000)
	
	_, err := s.Run()
	if err != nil {
		fmt.Errorf(err.Error())
	} else {
		fmt.Println("Gocho terminated properly.")
	}
}
