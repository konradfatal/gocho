//This package provides the server routines
package server

import (
	"bufio"
	log "code.google.com/p/log4go"
	"container/list"
	"errors"
	"fmt"
	"net"
	"strconv"
	"strings"
	"time"
)

//Server structure holds the server connection and the list of connections
type Server struct {
	port       int
	clientList *list.List
	listener   net.Listener
	running    bool
}

//Client structure wraps a single client connection
type Client struct {
	connection net.Conn
	buffer     []byte
	state      int
}

var (
	//The main singleton server
	mainServer *Server = nil
)

const (
	//The time interval when the server checks for new connectionss
	ServerPulse     = time.Second / 10
	//This state indicates that a client is connecteds
	STATE_CONNECTED = 0
	//This state indicates that a client connection is closing soons
	STATE_CLOSING   = 1
	//This state indicates that the client's buffer is pending to be written backs
	STATE_PENDING   = 2
	//The client buffers sizes
	channelSize     = 1024
)

//Creates a new server that will use the given port once started.
func New(port int) *Server {
	if mainServer == nil {
		mainServer = new(Server)
		mainServer.port = port
		mainServer.clientList = list.New()
		mainServer.listener = nil
		mainServer.running = false
	}

	return mainServer
}

//Runs the server on localhost
func (s *Server) Run() (bool, error) {

	ln, err := net.Listen("tcp", "127.0.0.1:"+strconv.Itoa(s.port))
	if err != nil {
		log.Error(err.Error())
		return false, err
	}
	defer ln.Close()

	log.Info("Gocho running on port " + strconv.Itoa(s.port))

	s.listener = ln

	go s.clientHandler()
	log.Info("ClientHandler dispatched...")
	s.startListening()

	return true, nil
}

//Closes all client connections and sets the flag to end the main server loop
func (s *Server) Close() (ok bool, err error) {
	if s.listener != nil {
		ok = true
		log.Info("Closing Gocho...")
		s.running = false

		//close all connections
		for cl := s.clientList.Front(); cl != nil; cl = cl.Next() {
			if c, ok := cl.Value.(*Client); ok {
				c.connection.Close()
			}
		}
		s.clientList.Init();

		//send dummy to close connection
		conn, err := net.Dial("tcp", s.listener.Addr().String())
		defer conn.Close()
		if err != nil {
			conn.Write([]byte{'\n'})
		}
	} else {
		err = errors.New("Server is not yet running")
		ok = false
	}

	return
}

//Creates a new client with the given connection.
func (s *Server) newClient(conn net.Conn) (c *Client) {
	c = new(Client)
	c.connection = conn
	c.buffer = make([]byte, channelSize)
	c.state = STATE_CONNECTED
	s.clientList.PushFront(c)
	
	out := bufio.NewWriter(conn)
	out.WriteString("To quit send 'xxx'\r\n");
	out.Flush()
	
	return
}

//Handles client connections
func (s *Server) clientHandler() {
	for s.running {
		for c := mainServer.clientList.Front(); c != nil; c = c.Next() {

			if cl, ok := c.Value.(*Client); ok {
				if cl.state == STATE_CLOSING {
					log.Info("Closing " + cl.connection.RemoteAddr().String())
					s.clientList.Remove(c)
					cl.connection.Close()
				} else if cl.state == STATE_PENDING {
					processClientOutput(cl)
				} else {
					go s.processClientInput(cl)
				}
			}
		}
		time.Sleep(time.Millisecond)
	}

	log.Info("ClientHandler terminated")
}

//Main server loop that waits for client connections
func (s *Server) startListening() {

	s.running = true
	for {

		log.Info("Waiting for a connection...")

		conn, err := s.listener.Accept()
		if err != nil {
			log.Error(err.Error())
			break
		}

		if !s.running {
			break
		}

		log.Info("Connection received from " + conn.RemoteAddr().String())
		s.newClient(conn)
		time.Sleep(time.Millisecond)
	}

	fmt.Println("Sever loop ended")
}

//Process incomming client inputs.
func (s *Server) processClientInput(cl *Client) {
	for s.running {
		line, err := bufio.NewReader(cl.connection).ReadBytes('\n')
		//disregard the error, we only want open connections
		if err == nil {
			cl.buffer = line
			cl.state = STATE_PENDING
			str := strings.TrimSpace(string(line))
			log.Info("RCVD: " + str)
			if strings.ToLower(str) == "xxx" {
				cl.state = STATE_CLOSING
				s.Close()
			}
		}
		time.Sleep(time.Millisecond)
	}
}

//Resends the client input back to it.
func processClientOutput(cl *Client) {
	out := bufio.NewWriter(cl.connection)
	out.Write(cl.buffer)
	out.Flush()
	cl.state = STATE_CONNECTED
	log.Info("SEND: " + string(cl.buffer))

}
